#!/usr/bin/env bash

# Add base constant

: "$GOOGLE_APPLICATION_CREDENTIALS"
export PROJECT="uraevsky"
export KOPS_STATE_STORE="gs://uraevsky-clusters/"
export ZONE="europe-west1-b"
export CLUSTER_NAME="uraevsky.k8s.local"
export KOPS_FEATURE_FLAGS=AlphaAllowGCE

create_cluster () {
	kops create cluster $CLUSTER_NAME --zones $ZONE --state $KOPS_STATE_STORE/ --project=$PROJECT
	kops update cluster --name $CLUSTER_NAME --yes
}

stop_cluster () {
	kops delete cluster $CLUSTER_NAME --yes
}

get_kubeconfig () {
	kops export kubecfg $CLUSTER_NAME
}

smoke_test () {
    echo Waited start cluster 210 seconds
    sleep 210

    max_wait=690

    while [ ${max_wait} -gt 0 ]; do
        kops validate cluster ${CLUSTER_NAME} --state ${KOPS_STATE_STORE} && break || sleep 30
        max_wait=$[max_wait - 30]
        echo Waited 30 seconds. Still waiting max. ${max_wait}
    done
    if [ ${max_wait} -le 0 ]; then
        echo Timeout: cluster does not validate after 15 minutes
        exit 1
    fi

    kubectl get all --all-namespaces
}

case "$1" in
    "start" )
        create_cluster
        get_kubeconfig
        smoke_test
        ;;

    "stop" )
        stop_cluster
        ;;

    *)
        echo "Wrong arguments"
        exit 1
esac
